#include <Arduino.h>

// Ingressi:
// Pulsante prenotazione pedone (D3, attivo alto)
//
// Uscite
// D0 -> ALTO: ROSSO PEDONE ON / VERDE MACCHINE ON
//       BASSO: VERDE PEDONE ON / ROSSO MACCHINE ON
// D1 -> ALTO: GIALLO PEDONE ON / BASSO: GIALLO PEDONE OFF
// D2 -> ALTO: GIALLO MACCHINE ON / BASSO: GIALLO MACCHINE OFF
// 
// Macchina a stati
// Stati:
//        VERDE_PEDONE -> D0 BASSO, D1 BASSO, D2 BASSO,
//                        START_TEMPORIZZAZIONE
//        GIALLO_PEDONE -> D0 BASSO, D1 ALTO, D2 BASSO,
//                         START TEMPORIZZAZIONE
//        ROSSO_PEDONE -> D0 ALTO, D1 BASSO, D2 BASSO
//        GIALLO_MACCHINE -> D0 ALTO, D1 BASSO, D2 ALTO,
//                         START TEMPORIZZAZIONE
//        ATTESA_PEDONE -> D0 ALTO, D1 BASSO, D2 BASSO,
//                         START TEMPORIZZAZIONE
//
// Eventi:
//        RICHIESTA_PEDONE -> PRESSIONE DEL BOTTONE RICHIESTA
//        SCADENZA_TIMER -> SCADENZA DI UNA TEMPORIZZAZIONE
//
// Macchina (X = EVENTO IGNORATO)
//
//        STATO         |     EVENTO       |     PROSSIMO STATO
//     -----------------+------------------+--------------------
//      ROSSO_PEDONE    | RICHIESTA_PEDONE | ATTESA_PEDONE
//                      | SCADENZA_TIMER   | X
//     -----------------+------------------+---------------------
//       ATTESA_PEDONE  | RICHIESTA_PEDONE | X
//                      | SCADENZA_TIMER   | GIALLO_MACCHINE
//     -----------------+------------------+---------------------
//       GIALLO_MACCHINE| RICHIESTA_PEDONE | X
//                      | SCADENZA_TIMER   | VERDE_PEDONE
//     -----------------+------------------+---------------------
//       VERDE_PEDONE   | RICHIESTA_PEDONE | X
//                      | SCADENZA_TIMER   | GIALLO_PEDONE
//     -----------------+------------------+---------------------
//       GIALLO_PEDONE  | RICHIESTA_PEDONE | X
//                      | SCADENZA_TIMER   | ROSSO_PEDONE
//     -----------------+------------------+---------------------

// Definizione stati
#define S_ROSSO_PEDONE      0
#define S_ATTESA_PEDONE     1
#define S_GIALLO_MACCHINE   2
#define S_VERDE_PEDONE      3
#define S_GIALLO_PEDONE     4

// Definizione eventi
#define E_RICHIESTA_PEDONE  1
#define E_SCADENZA_TIMER     2

/* Tutte le temporizzazioni sono da 5 secondi */
#define TEMPO_TIMER 5000
static unsigned long timer_next;
static int timer_running;

static int timer_start(void)
{
  if (timer_running)
    return -1;
  timer_next = millis() + TEMPO_TIMER;
  return 0;
}

// Chiamare da funzione loop
// Restituisce E_SCADENZA_TIMER se timer scaduto, 0 se timer non scaduto o fermo
static int timer_loop(void)
{ 
  if (!timer_running)
    return 0;
  if (millis() > timer_next) {
    timer_running = 0;
    return E_SCADENZA_TIMER;
  }
  return 0;
}

// Chiamare da funzione loop
// Restituisce E_RICHIESTA_PEDONE se pulsante premuto, 0 se pulsante rilasciato
static int leggi_pulsante_richiesta(void)
{
  if (digitalRead(D3))
    return E_RICHIESTA_PEDONE;
  return 0;
}

static int stato_corrente;

// Stati: queste funzioni vengono chiamate quando si entra nello stato
// corrispondente
static void stato_verde_pedone()
{
  stato_corrente = S_VERDE_PEDONE;

  // D0 BASSO, D1 BASSO, D2 BASSO
  digitalWrite(D0, 0);
  digitalWrite(D1, 0);
  digitalWrite(D2, 0);
  timer_start();
}

static void stato_giallo_pedone()
{
  stato_corrente = S_GIALLO_PEDONE;

  // D0 BASSO, D1 ALTO, D2 BASSO.
  digitalWrite(D0, 0);
  digitalWrite(D1, 1);
  digitalWrite(D2, 0);
  timer_start();
}

static void stato_rosso_pedone()
{
  stato_corrente = S_ROSSO_PEDONE;

  // D0 ALTO, D1 BASSO, D2 BASSO
  digitalWrite(D0, 1);
  digitalWrite(D1, 0);
  digitalWrite(D2, 0);
}

static void stato_giallo_macchine()
{
  stato_corrente = S_GIALLO_MACCHINE;

  // D0 ALTO, D1 BASSO, D2 ALTO
  digitalWrite(D0, 1);
  digitalWrite(D1, 0);
  digitalWrite(D2, 1);
  timer_start();
}

static void stato_attesa_pedone()
{
  stato_corrente = S_ATTESA_PEDONE;

  // D0 ALTO, D1 BASSO, D2 BASSO,
  digitalWrite(D0, 1);
  digitalWrite(D1, 0);
  digitalWrite(D2, 0);
  timer_start();
}

void setup() {
  pinMode(D0, OUTPUT);
  pinMode(D1, OUTPUT);
  pinMode(D2, OUTPUT);
  pinMode(D3, INPUT);
  stato_rosso_pedone();
}

static void esegui_macchina(int evento)
{
  switch(stato_corrente) {
    case S_ROSSO_PEDONE:
      if (evento == E_RICHIESTA_PEDONE)
        stato_attesa_pedone();
      break;
    case S_ATTESA_PEDONE:
      if (evento == E_SCADENZA_TIMER)
        stato_giallo_macchine();
      break;
    case S_GIALLO_MACCHINE:
      if (evento == E_SCADENZA_TIMER)
        stato_verde_pedone();
      break;
    case S_VERDE_PEDONE:
      if (evento == E_SCADENZA_TIMER)
        stato_giallo_pedone();
      break;
    case S_GIALLO_PEDONE:
      if (evento == E_SCADENZA_TIMER)
        stato_rosso_pedone();
      break;
    default:
      break;
  }
}

void loop() {
  int evento = timer_loop();

  evento |= leggi_pulsante_richiesta();

  if (evento & E_RICHIESTA_PEDONE)
    esegui_macchina(E_RICHIESTA_PEDONE);
  if (evento & E_SCADENZA_TIMER)
    esegui_macchina(E_SCADENZA_TIMER);
}